# README #

Word Jumble is an application that will take a word as input and output every possible combination of words.

The default dictionary used to compare words with is located in the data/ folder. It consits of a *.txt files which are a subset of word lists found here http://wordlist.aspell.net/. Each file consists of a list of words, one on each line.

A custom dictionary can be defined by adding passing the directory location using a -d argument. All dictionary files should have a .txt extension and each file must consist of a list of words, one on each line.

```
>>> python word_jumble.py -h
usage: word_jumble.py [-h] -w WORD [-d DATA_DIR] [-l LOG_LEVEL]

Accepts a string as input and returns a list of words that can be created
using the input letters

optional arguments:
  -h, --help            show this help message and exit
  -w WORD, --word WORD  Input word
  -d DATA_DIR, --data-dir DATA_DIR
                        The directory where all the data files with the
                        english words is stored
  -l LOG_LEVEL, --log-level LOG_LEVEL
                        Log Level

```

#####Example usage:

```
>>> python word_jumble.py -w curb 
rub
bur
cub
cur
curb
cru
bc
crub
ruc
ur
urb
rb
ru
ubc
```