#!/usr/bin/python
import logging
import io
import argparse
import glob
import os
import multiprocessing


def get_logger(log_level):
    """
    setup logging for the file
        Args:
          log_level (str): The intended loglevel (defaults to INFO)
    """
    formatter = logging.Formatter( "%(levelname)s - %(asctime)s -"
                                   " %(process)s - %(lineno)s: %(message)s" )
    logger = logging.getLogger()
    logger.setLevel(log_level.upper())
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(formatter)
    logger.addHandler(consoleHandler)
    return logger


def parse_options():
    """
    parse command line arguments
    """
    parser = argparse.ArgumentParser(description=
                                     'Accepts a string as input and'
                                     ' returns a list of words that'
                                     ' can be created using the '
                                     'input letters')
    parser.add_argument('-w','--word',
                        help="Input word",
                        type=str,
                        required=True)
    parser.add_argument('-d','--data-dir',
                        help='The directory where all'
                        ' the data files with the english words is stored',
                        default='data')
    parser.add_argument('-l','--log-level',
                        help='Log Level',
                        default='INFO')
    opt=parser.parse_args()
    return opt


def find_match(word1,word2):
    """Check the characters in word1 and see if it can be made from word2
        The logic behind this method is that if the characters of word1
         are all present in word2, it's a match for our purposes and
         word1 can be derived from word2

        Args:
          word1 (str): The word that can be made from the other word
          word2 (str): The root word from which the other word should be derived

        Example usage:

        >>> find_match('boo','bonobo')
        True
        >>> find_match('ball','bonobo')
        False
        >>> find_match('bonobo','boo')
        False
    """
    #create temp lists to keep track of what characters have been used
    word1_list = list(word1)
    word2_list = list(word2)
    #run through each character in the derived word
    for l in list(word1):
        try:
            if l in word2_list:
                #if the character exists in the root word, it's a match so far
                #remove it from the temp lists
                word1_list.remove(l)
                word2_list.remove(l)
        except ValueError:
            break
    #if all characters were present in word1, the temp list should be empty
    if len(word1_list) == 0 and len(str(word1)) > 1:
        return True
    #if the temp list was not empty, there were unmatched characters for word1
    # so it cannot be derived from word2
    return False


def worker(filename,input_word):
    """Run the actual comparison for an entire file

        Args:
          filename (str): The filename of a list of words to
            compare input_word to
          input_word (str): The word to jumble

    """
    logging.debug('Comparing {} with file {}'.format(input_word,filename))
    #use the io.open method so the code is python 2/3 compatible
    with io.open(filename, encoding = "ISO-8859-1") as f:
        for word in f:
            stripped_word = word.rstrip()
            if find_match(stripped_word,input_word):
                logging.info(stripped_word)


def get_data_files(data_dir):
    """
    get a list of dictionary files

        Args:
          data_dir (str):
            The data directory to fetch english words from
    """
    return [filename for filename in glob.glob(os.path.join(data_dir,
                                               '*.txt'))]


if __name__ == '__main__':
    opts = parse_options()
    logger = get_logger(opts.log_level)
    input_word = opts.word
    #set up a multiprocessing Pool
    pool = multiprocessing.Pool()
    data_files = get_data_files(opts.data_dir)
    for f in data_files:
        pool.apply_async(worker,args=(f,input_word))
    pool.close()
    pool.join()

